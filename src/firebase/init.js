import firebase from 'firebase/app';
import firestore from 'firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyCKPIMaLBqHLkmSd_ou1peWMxJRbkt5_Gw",
    authDomain: "peppy-tiger-231006.firebaseapp.com",
    databaseURL: "https://peppy-tiger-231006.firebaseio.com",
    projectId: "peppy-tiger-231006",
    storageBucket: "peppy-tiger-231006.appspot.com",
    messagingSenderId: "738307418423",
    appId: "1:738307418423:web:0300d341b8f7abd4ad92b2"
};
// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);
firebaseApp.firestore().settings({ timestampsInSnapshots: true });

export default firebaseApp.firestore();