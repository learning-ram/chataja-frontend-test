# chataja-frontend-test

## Perhatian!

Aplikasi ini menggunakan library fontawesome yang diambil dari CDN dan juga firebase sehingga memerlukan koneksi internet.

## Demo Aplikasi

[Demo](http://chataja-ram.surge.sh/)

User 1
1. user: jarjit@mail.com
2. pass: 123456

User 2
1. user: ismail@mail.com
2. pass: 123456

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
